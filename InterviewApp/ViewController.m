//
//  ViewController.m
//  InterviewApp
//
//  Created by Audun Follegg on 31.01.15.
//  Copyright (c) 2014 PicklePick AS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) Mojo *mojo;
@property (weak, nonatomic) IBOutlet MojoView *mojoView;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mojo = [[Mojo alloc] init];
    self.mojo.value = 100;
    [self.mojoView configureWithValue:self.mojo.value];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
