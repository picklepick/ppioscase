//
//  MojoView.m
//  InterviewApp
//
//  Created by Audun Follegg on 31.01.15.
//  Copyright (c) 2014 PicklePick AS. All rights reserved.
//

#import "MojoView.h"

@interface MojoView ()
@property (nonatomic) UILabel *label;
@end

@implementation MojoView

- (void)awakeFromNib
{
    self.label = [UILabel new];
}

- (void)configureWithValue:(NSUInteger)value
{
    self.label.font = [UIFont systemFontOfSize:50];
    self.label.text = [NSString stringWithFormat:@"%d", value];
    self.backgroundColor = [UIColor grayColor];
    self.label.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:self.label];
}

- (void)layoutSubviews
{
    [self.label sizeToFit];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.frame = CGRectInset(self.bounds, 10, 10);
}

@end
