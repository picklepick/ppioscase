//
//  Mojo.h
//  InterviewApp
//
//  Created by Audun Follegg on 31.01.15.
//  Copyright (c) 2014 PicklePick AS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mojo : NSObject
@property NSUInteger value;
@end
