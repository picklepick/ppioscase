//
//  main.m
//  InterviewApp
//
//  Created by Audun Follegg on 31.01.15.
//  Copyright (c) 2014 PicklePick AS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
