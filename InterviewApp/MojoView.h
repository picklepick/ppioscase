//
//  MojoView.h
//  InterviewApp
//
//  Created by Audun Follegg on 31.01.15.
//  Copyright (c) 2014 PicklePick AS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MojoView : UIView
- (void)configureWithValue:(NSUInteger)value;
@end
