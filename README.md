## IOS Interview: The MOJO app ##
*Requirement: We require a storyboard based solution*

Tasks 

1. Add the main view controller to a navigation controller

2. Make a button to push in a new view controller that holds a slidable slider 
3. The value of the slider must be between 1 and 100. The value should be used to update the value of the Mojo instance in the main view controller.
4. Explain how the navigation controller works 
5. Explain how the new view controller (with the slider) can update the mojo variable (in the main view controller) with the slider-value
6. Add a label under the slider that shows the value of the slider (1-100) and is updated when the slider is moved